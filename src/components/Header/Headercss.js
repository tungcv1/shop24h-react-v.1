export const headerPosition = {
    position: "fixed",
     zIndex: "1",
     backgroundColor: "#96D6FF",
     color: "white"
};
export const logo ={
    fontSize: "2.5rem",
     fontWeight: "bold",
      display: "inline-block",
}

export const textDecoration = {
    textDecoration: "none",
     color: "white"
}
export const styleButon = {
    paddingRight: "20px", 
    color: "white"
}
export const buttonSpace ={
    marginLeft: "50px", 
    marginBottom: "15px"
}


