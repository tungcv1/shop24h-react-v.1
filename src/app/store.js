import { createStore, combineReducers } from "redux";
import filterReducer from "../components/FilterReducer/filterReducer ";
import cartReducer from "../components/CartReducer/cartReducer";

const appReducer =combineReducers({
    filter: filterReducer,
    cart: cartReducer
});

const store = createStore(
    appReducer,undefined,undefined
);

export default store;