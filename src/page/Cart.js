import { Container, Table, TableHead, TableRow, TableCell, TableContainer, Paper, Grid, TableBody, Button, } from "@mui/material";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";

import { useEffect, useState } from "react";

function Cart() {
    const [order, setOrder] = useState([]);
    const [itemTotal, setItemTotal] = useState(0);
    const [selectItem, setSelectItem] = useState([])



    // Add/Remove checked item from list
    const onSelectAllItem = (e) => {
        let total = 0;
        let arraySelectedItem = [];
        if (e.target.checked) {
            order.map((element, index) => {
                total += element.price * element.amout
                arraySelectedItem.push(element)
                document.getElementById(element.id).checked = true
            })
        }
        else {
            order.map((element, index) => {
                document.getElementById(element.id).checked = false
            })
            total = 0;
            arraySelectedItem = []
        }
        setItemTotal(total);
        setSelectItem(arraySelectedItem);
    };

    const onSelectItem = (event) => {
        let total = itemTotal;
        let arraySelectedItem = selectItem;
        order.map((element, index) => {
            if (element.id === event.target.value) {
                if (event.target.checked) {
                    total += element.price * element.amout;
                    arraySelectedItem.push(element);
                }
                if (!event.target.checked) {
                    total -= element.price * element.amout;
                    arraySelectedItem.splice(index, 1);
                    document.getElementById("select-all-item").checked = false
                }
            }
        })
        setItemTotal(total);
        setSelectItem(arraySelectedItem);
    }

    const onBtnMinusAmountClick = (param) => {
        order.map((element, index) => {
            if (element.name === param.name) {
                element.amout = param.amout - 1;
            }

        })
    }



    const onDeleteOrderClick = (id) => {
        var newOrder = localStorage.getItem("order") ? JSON.parse(localStorage.getItem("order")) : [];
        newOrder.splice(id, 1);
        localStorage.setItem("order", JSON.stringify(newOrder));
        setOrder(newOrder);
    }


    useEffect(() => {
        setOrder(localStorage.getItem("order") ? JSON.parse(localStorage.getItem("order")) : [])

    }, [])

    return (
        < >
            <Header />
            <Container sx={{ paddingTop: "5rem", paddingBottom: "30rem" }}>
                <TableContainer component={Paper} >
                    <Table sx={{ minWidth: 650 }} aria-label=" table">
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    <input
                                        type="checkbox"
                                        className="form-check-input"
                                        name="allSelect"
                                        id="select-all-item"
                                        onChange={onSelectAllItem}
                                    />
                                </TableCell>
                                <TableCell>Sản Phẩm</TableCell>
                                <TableCell textAlign="right">Đơn Giá</TableCell>
                                <TableCell textAlign="right">Số Lượng</TableCell>
                                <TableCell textAlign="right">Số Tiền</TableCell>
                                <TableCell textAlign="right">Thao Tác</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                order.map((row, index) => {
                                    return <TableRow key={index}>
                                        <TableCell>
                                            <input
                                                type="checkbox"
                                                name={row.id}
                                                value={row.id}
                                                id={row.id}
                                                onChange={onSelectItem}
                                            />
                                        </TableCell>
                                        <TableCell>
                                            <Grid container spacing={2}>
                                                <Grid item xs={4}>
                                                    <img src={row.url} alt={index} style={{ maxWidth: "40%" }} />
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <h4>{row.name}</h4>
                                                </Grid>
                                            </Grid>
                                        </TableCell>
                                        <TableCell textAlign="center" >{row.price} $</TableCell>
                                        <TableCell textAlign="center" style={{ display: "flex", border: "none", paddingTop: "50px", alignItems: "center" }}>
                                            <button onClick={() => onBtnMinusAmountClick(row)}>-</button>{"\u00a0"}{row.amout}{"\u00a0"}<button>+</button>
                                        </TableCell>
                                        <TableCell align="center">{row.price * row.amout} $</TableCell>
                                        <TableCell align="center"><Button onClick={() => { onDeleteOrderClick(index) }}>Xóa</Button></TableCell>
                                    </TableRow>

                                })
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
                <Grid container mt={4}>
                    <Grid item xs={2} pl={2}>
                        <input
                            type="checkbox"
                            className="form-check-input"
                            name="allSelect"
                            id="select-all-item"
                            onChange={onSelectAllItem}
                        />
                        Chọn tất cả ({order.length})
                    </Grid>
                    <Grid item xs={7} textAlign="center" style={{ marginTop: "2px" }}>
                        <h2>Tổng thanh toán ({itemTotal} $):</h2>
                    </Grid>
                    <Grid item xs={3} textAlign="right">
                        <Button variant="contained" fullWidth>ĐẶT HÀNG</Button>
                    </Grid>
                </Grid>
            </Container>
            <Footer />
        </>
    );
}
export default Cart;